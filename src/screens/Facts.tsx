import React from 'react';
import {Text, View} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// const FactDetailsScreen = () => {
//   return (
//     <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
//       <Text>Dog Facts screen</Text>
//     </View>
//   );
// };

const FactsListScreen = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Dog Facts screen</Text>
    </View>
  );
};

export const FactsScreen = () => {
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer independent={true}>
      <Stack.Screen name="Quiz!" component={FactsListScreen} />
    </NavigationContainer>
  );
};
