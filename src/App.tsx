import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {FactsScreen, QuizScreen} from './screens';

const App = () => {
  const Tab = createBottomTabNavigator();

  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Facts">
        <Tab.Screen
          name="Facts"
          component={FactsScreen}
          options={{tabBarTestID: 'Facts Tab'}}
        />
        <Tab.Screen
          name="Quiz"
          component={QuizScreen}
          options={{tabBarTestID: 'Quiz Tab'}}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
