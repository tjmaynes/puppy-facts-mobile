install:
	./scripts/install.sh

.PHONY: test
unit_test:
	npm test

ui_test_android:
	./scripts/ui-tests.sh "android"

ui_test_ios:
	./scripts/ui-tests.sh "ios"

test: unit_test ui_test_ios ui_test_android

start_metro:
	npm start

start_ios:
	npm run ios

start_android:
	npm run android

start_wiremock:
	./wiremock/start.sh

stop_wiremock:
	./wiremock/stop.sh

download_android_emulator:
	./scripts/download-android-emulator.sh

run_android_emulator:
	./scripts/run-android-emulator.sh

ship_it:
	./scripts/ship-it.sh

build_ios:
	./scripts/build-ios.sh

build_android:
	./scripts/build-android.sh

build: build_ios build_android

deploy_android: install unit_test ui_test_android

deploy_ios: install unit_test ui_test_ios

deploy: deploy_ios deploy_android

stop_debugger:
	kill -9 $(lsof -t -i:8081)
