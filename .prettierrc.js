module.exports = {
  printWidth: 80,
  tabWidth: 2,
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'es5',
  arrowParens: 'avoid',
  semi: true
};
