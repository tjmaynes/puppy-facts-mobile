#!/bin/bash

set -e

PORT=8888

function main() {
    java -jar wiremock/wiremock-2.30.1.jar --port "$PORT" &

    sleep 2

    curl --location --request POST "http://localhost:$PORT/__admin/mappings" \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "request": {
                "urlPattern": "/ready",
                "method": "GET"
            },
            "response": {
                "status": 200
            }
        }' \
        --silent --output /dev/null

    while ! curl --output /dev/null --silent --fail "http://localhost:8888/ready"; do sleep 1 && echo -n .; done;
}

main