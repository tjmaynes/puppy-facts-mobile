#!/bin/bash

set -e

PORT=8888

function main() {
    kill -9 $(lsof -t -i:$PORT)
}

main