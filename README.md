# Puppy Facts
> A React Native application for learning interesting dog facts.

## Requirements

- [GNU Make](https://www.gnu.org/software/make/)
- [Xcode](https://developer.apple.com/xcode/)
- [Android Studio](https://developer.android.com/studio)
- [Nodejs](https://nodejs.org/en/)
- [Ruby](https://www.ruby-lang.org/en/)
- [Bundler](https://bundler.io/)

## Usage
To install project dependencies, run the following command:
```bash
make install
```

To run all tests, run the following command:
```bash
make test
```

To debug ios and android apps via metro, run the following command:
```bash
make start_metro
```

To run ios, run the following command:
```bash
make start_ios
```

To run android, run the following command:
```bash
make start_android
```

To ship the app, run the following command:
```bash
make ship_it
```

To deploy the app, run the following command:
```bash
make deploy
```

## Helpful links

### React Native on M1
I've mostly been having quite a few problems setting up this project on my M1 Mac Mini. I'm thinking that I'll have to open Xcode, Alacritty, and VSCode in Rosetta mode to make this easier (it's just not there yet). - TJ Maynes (08-30-2021)

- [Unable to build via 'xcodebuild' and run via 'react-native run'](https://github.com/facebook/react-native/issues/29605)
- [React Native - Xcode 12.5 troubleshooting guide](https://github.com/facebook/react-native/issues/31480)
- [React Native on M1 Guide](https://github.com/aiba/react-native-m1/blob/main/README.md)

### E2E testing via Detox
- [Detox Android setup guide](https://github.com/wix/Detox/blob/master/docs/Introduction.Android.md)
- [E2E Testing with Detox](https://blog.logrocket.com/end-to-end-testing-in-react-native-with-detox/)

### Other
- [React Native Detox GH Actions example project](https://github.com/edvinasbartkus/react-native-detox-github-actions)
- [Deploy OTA changes via CodePush](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/)