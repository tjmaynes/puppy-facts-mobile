#!/bin/bash

set -e

EMULATOR_NAME="test-avd"

function check_requirements() {
    if [[ -z "$EMULATOR_NAME" ]]; then
        echo "Please provide an emulator name to create an emulator with."
        exit 1
    elif [[ -z "$(command -v $ANDROID_HOME/tools/bin/sdkmanager)" ]]; then
        echo "Please install Android SDK tools: sdkmanager"
        exit 1
    elif [[ -z "$(command -v $ANDROID_HOME/tools/bin/avdmanager)" ]]; then
        echo "Please install Android SDK tools: avdmanager"
        exit 1
    fi
}

function main() {
    check_requirements

    ARCH="$(uname -m)"
    [ "$ARCH" = "x86_64" ] && ARCH="arm64-v8a"

    PACKAGES="system-images;android-30;google_apis;$ARCH"

    yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses
    echo "y" | $ANDROID_HOME/tools/bin/sdkmanager --install "$PACKAGES"
    echo "no" | $ANDROID_HOME/tools/bin/avdmanager create avd --force --name "$EMULATOR_NAME" --device "pixel" -k "$PACKAGES"

    if ! $ANDROID_HOME/emulator/emulator -list-avds  | grep -q "$EMULATOR_NAME" ; then
        echo "The emulator '$EMULATOR_NAME' was not created!"
        exit 1
    fi
}

main