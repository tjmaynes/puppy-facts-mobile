#!/bin/bash

set -e

TARGET=$1

function check_requirements() {
    if [[ -z "$TARGET" ]]; then
        echo "Please  provide an argument for the 'TARGET': ios or android."
        exit 1
    fi
}

function run_e2e_tests() {
    detox build --configuration $1
    detox test --configuration $1
}

function main() {
    ./wiremock/start.sh

    if [ "$TARGET" = "android" ]; then
        ./scripts/run-android-emulator.sh
    fi

    run_e2e_tests "$TARGET" || {
        ./wiremock/stop.sh
        exit 1
    }

    ./wiremock/stop.sh
}

main