#!/bin/bash

set -e

EMULATOR_NAME="test-avd"

function check_requirements() {
    if [[ -z "$EMULATOR_NAME" ]]; then
        echo "Please provide an emulator name to create an emulator with."
        exit 1
    elif [[ -z "$(command -v $ANDROID_HOME/tools/bin/sdkmanager)" ]]; then
        echo "Please install Android SDK tools: sdkmanager"
        exit 1
    elif [[ -z "$(command -v $ANDROID_HOME/tools/bin/avdmanager)" ]]; then
        echo "Please install Android SDK tools: avdmanager"
        exit 1
    elif ! $ANDROID_HOME/emulator/emulator -list-avds  | grep -q "$EMULATOR_NAME" ; then
        echo "The emulator '$EMULATOR_NAME' was not created!"
        exit 1
    fi
}

function main() {
    check_requirements

    echo "Starting emulator"

    nohup $ANDROID_HOME/emulator/emulator -avd "$EMULATOR_NAME" -no-audio -no-snapshot -no-window &
    $ANDROID_HOME/platform-tools/adb wait-for-device shell 'while [[ -z $(getprop sys.boot_completed | tr -d '\r') ]]; do sleep 1; done; input keyevent 82'

    $ANDROID_HOME/platform-tools/adb devices
    
    echo "Emulator started"
}

main