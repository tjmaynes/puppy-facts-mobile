#!/bin/bash

set -e

function main() {
  if [[ -n "$(git status --porcelain)" ]]; then 
    echo "You have uncommitted changes!"
    exit 1
  fi

  make test

  git push origin main
}

main
