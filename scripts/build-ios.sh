#!/bin/bash

set -e

function check_requirements() {
    if [[ -z "$(command -v bundle exec xcpretty)" ]]; then
        echo "Please provide the 'xcpretty' tool by running bundle install."
        exit 1
    fi
}

function main() {
    check_requirements

    CONFIGURATION=Release
    [ "$ENVIRONMENT" = "test" ] && CONFIGURATION=Debug

    xcodebuild clean build \
        -workspace ./ios/PuppyFacts.xcworkspace \
        -scheme PuppyFacts \
        -configuration "$CONFIGURATION" \
        -destination 'platform=iOS Simulator,name=iPhone 11,OS=14.5' \
        -derivedDataPath ./ios/build | bundle exec xcpretty
}

main