#!/bin/bash

set -e

function install_detox_dependencies() {
    [[ -z "$(command -v detox)" ]] && npm install --global detox-cli

    if [[ -z "$(command -v applesimutils)" ]]; then
        brew tap wix/brew
        brew install applesimutils
    fi
}

function install_node_dependencies() {
    [[ -z "$(command -v react-native)" ]] && npm install --global react-native

    npm install
}

function install_ios_dependencies() {
    [[ -z "$(command -v bundle)" ]] && gem install bundler --no-document

    bundle config path vendor/bundle
    bundle install

    install_detox_dependencies

	pushd ios
    bundle exec pod install
    popd
}

function install_android_dependencies() {
    yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses

    pushd android
    ./gradlew build
    popd
}

function main() {
    install_node_dependencies
    install_ios_dependencies
    install_android_dependencies
}

main