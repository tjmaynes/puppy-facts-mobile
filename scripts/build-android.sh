#!/bin/bash

set -e

function main() {
    BUILD_COMMAND="bundleRelease"
    [ "$ENVIRONMENT" = "ui-debug" ] && BUILD_COMMAND="assembleDebug"
    [ "$ENVIRONMENT" = "ui-release" ] && BUILD_COMMAND="assembleReleaseE2E"

    TEST_BUILD_TYPE="release"
    [ "$ENVIRONMENT" = "ui-debug" ] && TEST_BUILD_TYPE="debug" 

    pushd android
    ./gradlew "$BUILD_COMMAND" assembleAndroidTest -DtestBuildType="$TEST_BUILD_TYPE"
    popd
}

main