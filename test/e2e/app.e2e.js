describe('PuppyFacts', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should show multiple tabs', async () => {
    await expect(element(by.id('Facts Tab'))).toBeVisible();
    await expect(element(by.id('Quiz Tab'))).toBeVisible();
  });
});
